const TEST_CASES = [
  { input: "5+3", answer: "8" },
  { input: "5-6*4+(12/3)", answer: "-15" },
  { input: "(3*((2+1))-12/5)", answer: "6.6" },
  { input: "sin(32-cos(16))", answer: "0.999577689630313013" },
  { input: "cos(pi)/sin(11.5-pi)", answer: "-1.142266852390763514" },
  {
    input: "5(sin(3)-cos(pi*15))/4",
    answer: "-0.785099269487286377",
    degree: true,
  },
  {
    input: "3*(resin(4)-sin(12))",
    answer: "4714.176264927546864615",
    degree: true,
  },
  { input: "3=5", answer: "!ERROR" },
];

TEST_CASES.map(({ input, answer, degree }) => {
  it(`should caluculate ${input} equals to ${answer}`, function () {
    $input.value = input;
    degree && ($degree.checked = true);
    calculate();
    assert($answer.innerText === answer);
  });
});

// utilities

function it(desc, fn) {
  try {
    fn();
    console.log("\x1b[32m%s\x1b[0m", "\u2714 " + desc);
  } catch (error) {
    console.log("\n");
    console.log("\x1b[31m%s\x1b[0m", "\u2718 " + desc);
    console.error(error);
  } finally {
    afterAll();
  }
}

function assert(condition) {
  if (!condition) {
    throw new Error();
  }
}

function afterAll() {
  $input.value = "";
  $answer.innerText = "";
  $answer.style.color = "#2C2C2C";
  $answer.style.border = "2px solid #2C2C2C";
  $degree.checked = false;
}
