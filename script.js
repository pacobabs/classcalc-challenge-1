nerdamer.setFunction("resin", ["x"], "x*392.9");

const $input = document.getElementById("inputBox");
const $degree = document.getElementById("degree-checkbox");
const $answer = document.getElementById("answerParagraph");

$input.style.border = "2px solid #2C2C2C";
$answer.style.border = "2px solid #2C2C2C";

const toDegreeMode = (expression, isCos = false) => {
  const regex = isCos ? /\s?\bcos\b\(.*?\)\s*/g : /\s?\bsin\b\(.*?\)\s*/g;
  const matches = expression.match(regex);
  matches &&
    matches.map((match) => {
      const value = match.match(/\(([^)]+)\)/)[1];
      expression = expression.replace(
        match,
        `${isCos ? "cos" : "sin"}(${value}*(pi/180))`
      );
    });
  return expression;
};

const calculate = () => {
  try {
    const isDegreeMode = $degree.checked;
    const e = nerdamer(
      isDegreeMode
        ? toDegreeMode(toDegreeMode($input.value, true))
        : $input.value
    ).evaluate();
    $answer.innerText = $input.value ? e.text() : "";
    $answer.style.color = "#2C2C2C";
    $answer.style.border = "2px solid #2C2C2C";
  } catch {
    $answer.innerText = "!ERROR";
    $answer.style.color = "#EC9C8C";
    $answer.style.border = "2px solid #EC9C8C";
  }
};

$input.onkeyup = calculate;
$degree.onchange = calculate;
